$(document).ready(function() {
    function getAllShedule ($id) {
        $.get('/animal/'+ $id +'/schedule', function(response) {
            var tr = '';
            $.each(response, function(i,e) {
                var date = e.dt_service.split('-').reverse().join('/');
                var bo_notification = '';
                if (e.bo_notification) {
                    bo_notification = '<i class="ni ni-chat-round ni-2x text-info"></i>';
                }

                tr +=   '<tr>' +
                            '<td class="text-center">'+bo_notification+'</td>'+
                            '<td>'+date+'</td>'+
                            '<td>'+e.no_type+'</td>' +
                            '<td>'+e.st_note+'</td>' +
                            '<td class="text-center">'+
                                '<a class="archive-modal" data-id="'+e.id+'">' +
                                    '<i class="ni ni-check-bold text-success ni-2x"></i>' +
                                '</a>'+
                            '</td>' +
                            '<td class="text-center">'+
                                '<a class="delete-modal" data-id="'+e.id+'">' +
                                    '<i class="ni ni-fat-remove text-danger ni-2x"></i>' +
                                '</a>'+
                            '</td>'
            });

            $('#animal-schedules').html(tr);

            $('.archive-modal').off('click').on('click', function() {
                $('input[name=archive_schedule]').val($(this).attr('data-id'));
                $('#schedule-archive-modal').modal();
            });

            $('.delete-modal').on('click', function() {
                $('input[name=delete_schedule]').val($(this).attr('data-id'));
                $('#schedule-delete-modal').modal();
            });
        });
        $('#form-schedule')[0].reset();
    }

    $('#form-schedule').on('submit', function (e) {
        e.preventDefault();
        var data = $('#form-schedule').serialize();

        $.ajax({
            type: 'post',
            url: '/animal/'+$('#id_animal').val()+'/schedule',
            data: data,
            success: function () {
                getAllShedule($('#id_animal').val());

            }
        });
    });

    $('.archive-modal').on('click', function() {
        $('input[name=archive_schedule]').val($(this).attr('data-id'));
        $('#schedule-archive-modal').modal();
    });

    $('#button-archive-schedule').on('click', function (e) {
        e.preventDefault();
        var data = {
            "_token": $('meta[name="csrf-token"]').attr('content')
        };

        $.ajax({
            type: 'put',
            url: '/schedule/archive/'+$('input[name=archive_schedule]').val(),
            data: data,
            success: function () {
                getAllShedule($('#id_animal').val());
            }
        });
    });

    $('.delete-modal').on('click', function() {
        $('input[name=delete_schedule]').val($(this).attr('data-id'));
        $('#schedule-delete-modal').modal();
    });

    $('#button-delete-schedule').on('click', function (e) {
        e.preventDefault();
        var data = {
            "_token": $('meta[name="csrf-token"]').attr('content')
        };
        $.ajax({
            type: 'put',
            url: '/schedule/delete/'+$('input[name=delete_schedule]').val(),
            data: data,
            success: function () {
                getAllShedule($('#id_animal').val());
            }
        });
    });
});