$(document).ready(function() {
    $('#id_specie').on('change', function() {
        $.get('/animals/'+$(this).val()+'/breed', function(response) {
            var options = '<option value=""> Selecione a raça</option>';

            $.each(response, function(i,e) {
                options += '<option value="'+e.id+'">'+e.st_name+'</option>';
            });

            $('select[name=id_breed]').html(options);
        });
    });
});
