<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
    Route::resource('user', 'UserController', ['except' => ['show']]);
    Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
    Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
    Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
    Route::get('animals/{id_specie}/breed', '\App\Http\Controllers\Animals\BreedController@search');
    Route::get('animal/{id_animal}/services', '\App\Http\Controllers\Animals\ServiceController@search');

    Route::get('/home',                 'HomeController@index')->name('home');
    Route::get('animal/register',       'Animals\\AnimalController@form')->name('animal.register');
    Route::put('animal/save',           'Animals\\AnimalController@save')->name('animal.save');
    Route::patch('animal/save',         'Animals\\AnimalController@save')->name('animal.save');
    Route::get('animal/list',           'Animals\\AnimalController@list')->name('animal.list');
    Route::get('animal/detail/{id}',    'Animals\\AnimalController@detail')->name('animal.detail');
    Route::get('animal/edit/{id?}',     'Animals\\AnimalController@edit')->name('animal.edit');

//    Route::get('animal/service/{id?}',  'Animals\\ServiceController@index')->name('animal.service');
    Route::post('animal/{id}/services',  '\App\Http\Controllers\Animals\ServiceController@save');

    Route::get('animal/{id}/schedule',  '\App\Http\Controllers\Animals\ScheduleController@listSchedule');
    Route::post('animal/{id}/schedule', '\App\Http\Controllers\Animals\ScheduleController@save');
    Route::put('schedule/archive/{id}', '\App\Http\Controllers\Animals\ScheduleController@archive');
    Route::put('schedule/delete/{id}', '\App\Http\Controllers\Animals\ScheduleController@delete');

    Route::get('animal/schedule',       'Animals\\ScheduleController@listAll')->name('schedule.list');
    Route::get('animal/allSchedule',       'Animals\\ScheduleController@refreshListAll');

    Route::post('animal/medical-record/{idMedicalRecord}',  '\App\Http\Controllers\Animals\MinuteRecordController@save');
    Route::get('animal/medical-record/{idMedicalRecord}/minutes',  '\App\Http\Controllers\Animals\MinuteRecordController@list');

    Route::get('people/list', 'People\\PeopleController@list')->name('people.list');
    Route::get('people/register', 'People\\PeopleController@form')->name('people.register');
    Route::put('people/save', 'People\\PeopleController@save')->name('people.save');
});


Auth::routes();


