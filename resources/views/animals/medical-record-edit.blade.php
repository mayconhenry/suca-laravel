<div class="tab-pane" id="tabs-attendance" role="tabpanel">
    <h1>Editar prontuário</h1>
    <div class="dropdown-divider"></div>
    <form method="post" action="{{ route('animal.save') }}" autocomplete="off">
        @csrf
        @method('put')

        <div class="row space-under">
            <div class="col-4">
                <div class="form-group">
                    <label class="form-control-label" for="input-name">{{ __('Dia da chegada') }}</label>
                    <input type="text" name="st_name" id="input-name" class="form-control form-control-alternative" required autofocus>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label class="form-control-label" for="input-name">{{ __('Hora') }}</label>
                    <input type="text" name="st_name" id="input-name" class="form-control form-control-alternative">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label class="form-control-label" for="input-name">{{ __('Previsão de alta') }}</label>
                    <input type="text" name="st_name" id="input-name" class="form-control form-control-alternative">
                </div>
            </div>
        </div>
        <div class="row space-under">
            <div class="col-4">
                <div class="form-group">
                    <label class="form-control-label" for="input-name">{{ __('Consciente') }}</label>
                    <input type="text" name="st_name" id="input-name" class="form-control form-control-alternative">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label class="form-control-label" for="input-name">{{ __('Peso') }}</label>
                    <input type="text" name="st_name" id="input-name" class="form-control form-control-alternative">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label class="form-control-label" for="input-name">{{ __('Box') }}</label>
                    <input type="text" name="st_name" id="input-name" class="form-control form-control-alternative">
                </div>
            </div>
        </div>
        <div class="row space-under">
            <div class="col-12">
                <div class="form-group">
                    <label class="form-control-label" for="input-name">{{ __('Sintomas apresentados') }}</label>
                    <textarea class="form-control rounded-0" rows="5"></textarea>
                </div>
            </div>
        </div>


        <div class="text-right">
            <a href="{{ route('animal.list') }}" class="btn btn-danger btn-sm">{{ __('Cancelar') }}</a>
            <button type="submit" class="btn btn-success btn-sm">{{ __('Salvar') }}</button>
        </div>
    </form>
</div>