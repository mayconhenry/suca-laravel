@extends('layouts.app')

@section('content')
    @include('animals.header', [
        'title' => __(''),
        'class' => 'col-lg-7'
    ])

    <div class="container-fluid mt--7">
        <div class="row space-under">
            <div class="col-xl-10 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <h1>{{$animal->st_name_animal}}</h1>
                            </div>
                            <div class="col-md-6 text-right">
                                <a href="{{ route('animal.edit', ['id'=> $animal->id]) }}"><i class="fa fa-pen fa"></i> Editar</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row space-under">
                            <div class="col-md-4">
                                <label>Responsável</label>
                                <h2>{{$animal->st_name_owner}}</h2>
                            </div>
                            <div class="col-md-4">
                                <label>Espécie</label>
                                <h2>{{$animal->no_specie}}</h2>
                            </div>
                            <div class="col-md-4">
                                <label>Data de nascimento</label>
                                <h2>{{date('d/m/Y', strtotime($animal->dt_birth))}}</h2>
                            </div>
                        </div>
                        <div class="row space-under">
                            <div class="col-md-4">
                                <label>Contato</label>
                                <h2>{{$animal->nu_phone}}</h2>
                            </div>
                            <div class="col-md-4">
                                <label>Raça</label>
                                <h2>{{$animal->no_breed}}</h2>
                            </div>
                            <div class="col-md-4">
                                <label>Peso</label>
                                <h2>{{$animal->nu_weight}}kg</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row space-under">
            <div class="col-xl-10 ">
                <div class="card bg-secondary shadow">
                    <div class="card-body">
                        <div class="nav-wrapper">
                            <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0 active"
                                       id="tabs-icons-text-1-tab"
                                       data-toggle="tab"
                                       href="#tabs-attendance"
                                       role="tab"
                                       aria-controls="tabs-icons-text-2"
                                       aria-selected="true"
                                    >
                                        <i class="fa fa-concierge-bell mr-2"></i>Atendimento
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0"
                                       id="tabs-icons-text-2-tab"
                                       data-toggle="tab"
                                       href="#tabs-schedule"
                                       role="tab"
                                       aria-controls="tabs-icons-text-2"
                                       aria-selected="false"
                                    >
                                        <i class="ni ni-bell-55 mr-2"></i>Lembrete
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0"
                                       id="tabs-icons-text-3-tab"
                                       data-toggle="tab"
                                       href="#tabs-icons-text-3"
                                       role="tab"
                                       aria-controls="tabs-icons-text-3"
                                       aria-selected="false"
                                    >
                                        <i class="fa fa-clipboard mr-2"></i>Prontuário
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="card shadow">
                            <div class="card-body" style="height: 600px">
                                <div class="tab-content" id="myTabContent">

                                    {{--TAB DE ATENDIMENTO--}}
                                    @include('animals.service')

                                    {{--TAB DE LEMBRETE--}}
                                    @include('animals.schedule')

                                    {{--TAB DE PRONTUÁRIO--}}
                                    @include('animals.medical-record')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extra-scripts')
    <script src="{{ asset('js/animals/animalServiceForm.js') }}"></script>
    <script src="{{ asset('js/animals/animalScheduleForm.js') }}"></script>
    <script src="{{ asset('js/animals/animalMinuteForm.js') }}"></script>
@endsection
