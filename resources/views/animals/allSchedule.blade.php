@extends('layouts.app')

@section('content')
    @include('animals.header', [
        'title' => __(''),
        'class' => 'col-lg-7'
    ])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-10 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <h1 class="col-12 mb-0">{{ __('Lista de todos lembretes') }}</h1>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-white">
                            <thead>
                                <tr>
                                    <th scope="col" class="text-center">Notificação</th>
                                    <th scope="col">Data</th>
                                    <th scope="col">Nome</th>
                                    <th scope="col">Tipo de atendimento</th>
                                    <th scope="col">Detalhamento</th>
                                    <th scope="col" class="text-center">Contato</th>
                                    <th scope="col" class="text-center">Arquivar</th>
                                    <th scope="col" class="text-center">Excluir</th>
                                </tr>
                            </thead>
                            <tbody id="animal-schedules">
                                @foreach($arrSchedule as $key => $schedule)
                                    <tr>
                                        <td class="text-center">
                                            @if($schedule->bo_notification)
                                                <i class="ni ni-chat-round ni-2x text-info"></i>
                                            @endif
                                        </td>
                                        <td>{{date('d/m/Y', strtotime($schedule->dt_service))}}</td>
                                        <td>{{$schedule->no_animal}}</td>
                                        <td>{{$schedule->no_type}}</td>
                                        <td>{{$schedule->st_note}}</td>
                                        <td class="text-center">
                                            <a data-toggle="modal" data-target="#contact-{{$schedule->id}}" href="#">
                                                <i class="fa fa-address-card text-primary ni-2x"></i>
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <a class='archive-modal' data-id="{{$schedule->id}}">
                                                <i class="ni ni-check-bold text-success ni-2x"></i>
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <a class='delete-modal' data-id="{{$schedule->id}}">
                                                <i class="ni ni-fat-remove text-danger ni-2x"></i>
                                            </a>
                                        </td>

                                        <!-- Modal para contato com proprietário -->
                                        <div class="modal fade" id="contact-{{$schedule->id}}">
                                            <div class="modal-dialog modal-dialog-centered" >
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <div class="col-md-10" style="margin-top: 17px;">
                                                            <div class="col-md-12">
                                                                <label>Responsável</label>
                                                                <h2 style="margin-top: -10px;">{{$schedule->no_owner}}</h2>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <label>Telefone:</label>
                                                                <h2 style="margin-top: -10px;">{{$schedule->nu_phone}}</h2>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 text-right">
                                                            <a href="" data-dismiss="modal" style="color: black; font-size: 30px">x</a>
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <a href="{{route('animal.detail', ['id'=> $schedule->id_animal])}}" class="btn btn-info btn-sm">Cadastro Completo</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </tr>
                                @endforeach
                                <!-- Modal para confirmar arquivamento -->
                                <div class="modal fade" id="schedule-archive-modal">
                                    <div class="modal-dialog modal-dialog-centered" >
                                        <div class="modal-content align-items-center">
                                            <div class="modal-header" >
                                                <h5 class="modal-title" >Deseja realmente arquivar o agendamento?</h5>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="hidden" name="archive_schedule">
                                                <button type="button" class="btn btn-success btn-sm" data-dismiss="modal" id="button-archive-schedule">Sim</button>
                                                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Não</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Modal para confirmar delete -->
                                <div class="modal fade" id="schedule-delete-modal">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content align-items-center">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Deseja realmente apagar o agendamento?</h5>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="hidden" name="delete_schedule">
                                                <button type="button" class="btn btn-success btn-sm" data-dismiss="modal" id="button-delete-schedule">Sim</button>
                                                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Não</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extra-scripts')
    <script src="{{ asset('js/animals/animalAllScheduleForm.js') }}"></script>
@endsection
