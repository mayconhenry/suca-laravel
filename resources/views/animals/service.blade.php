<div class="tab-pane fade show active" id="tabs-attendance" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
    <form method="post" id="form-service">
        <input type="hidden" id="id_animal" name="id_animal" value="{{$id}}" >
        @csrf
        @method('post')
        <div class="row">
            <div class="col-3">
                <div class="form-group">
                    <label class="form-control-label" for="input-name">{{ __('Tipo de atendimento') }}</label>
                    <select name="id_type" id="id_type" class="form-control">
                        <option></option>
                        @foreach($arrServiceType as $serviceType)
                            <option value={{$serviceType->id}}>{{$serviceType->st_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-7">
                <div class="form-group">
                    <label class="form-control-label" for="input-name">{{ __('Observação') }}</label>
                    <input type="text" name="st_note" id="st_note" class="form-control form-control-alternative" required autofocus>
                </div>
            </div>
            <div class="col-2 text-center" style="margin-top: 40px">
                <button type="submit" class="btn btn-success btn-sm" >{{ __('Lançar') }}</button>
            </div>

        </div>
    </form>
    <table class="table table-white">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Tipo de atendimento</th>
            <th scope="col">Data</th>
            <th scope="col">Observação</th>
        </tr>
        </thead>
        <tbody id="animals_services">
            @foreach($arrService as $key => $service)
                <tr>
                    <th >{{$key + 1}}</th>
                    <td>{{$service->no_type}}</td>
                    <td>{{date('d/m/Y', strtotime($service->dt_service))}}</td>
                    <td>{{$service->st_note}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
{{--FIM DA TAB DE ATENDIMENTO--}}

