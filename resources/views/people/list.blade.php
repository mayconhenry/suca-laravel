@extends('layouts.app')

@section('content')
    @include('people.header', [
        'title' => __(''),
        'class' => 'col-lg-7'
    ])

    <div class="container-fluid mt--7">
        <div class="row space-under">
            <div class="col-xl-10 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <h3 class="col-12 mb-0">{{ __('Clientes') }}</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-white">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nome</th>
                                    <th scope="col">Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($arrClient as $key => $client)
                                    <tr>
                                        <th scope="row">{{$key + 1}}</th>
                                        <td>{{$client->st_name}}</td>
                                        <td>
                                            <a href="#" class="btn btn-primary btn-sm"><i class="fas fa-user-edit"></i></a>
                                            <a href="#" class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></a>
                                            <a href="#" class="btn btn-info btn-sm" data-toggle="modal" data-target="#detail-client"><i class="fas fa-info-circle"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="detail-client" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detalhes do cliente</h5>

                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label class="form-control-label" for="input-name">{{ __('Nome') }}</label>
                                <input type="text" name="st_name" id="input-name" class="form-control form-control-alternative" placeholder="{{ __('Nome') }}" required autofocus>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Sair</button>
                </div>
            </div>
        </div>
    </div>
@endsection