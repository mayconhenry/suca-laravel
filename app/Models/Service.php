<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Service extends Model
{
    public function fetchAll($id)
    {
        return DB::table('service')
                ->leftJoin('service_type', 'service.id_type', '=', 'service_type.id')
                ->where('id_animal', $id)
                ->get([
                    'service.id',
                    'service.st_note',
                    'service.dt_service',
                    'service_type.st_name as no_type'
                ]);
    }

    public function register($service)
    {
        try {
            DB::beginTransaction();
            DB::table('service')->insert($service);
            DB::commit();
            return [
                'success' => true,
                'message' => 'Sucesso ao salvar'
            ];
        } catch (Exception $e) {
            DB::rollback();
            return [
                'success' => false,
                'message' => 'Falha ao salvar'
            ];
        }
    }
}
