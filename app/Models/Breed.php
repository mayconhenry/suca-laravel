<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Breed extends Model
{
    public function fetchAll()
    {
        return DB::table('breed')->get();
    }

    public function searchForSpecie($specieId)
    {
        return DB::table('breed')->where('id_specie', $specieId)->get();
    }
}
