<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Species extends Model
{
    public function fetchAll()
    {
        return DB::table('species')->get();
    }
}
