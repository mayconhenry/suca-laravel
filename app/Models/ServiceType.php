<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ServiceType extends Model
{
    public function fetchAll()
    {
        return DB::table('service_type')->get();
    }
}
