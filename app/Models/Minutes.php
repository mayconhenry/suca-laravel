<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Minutes extends Model
{
    public function fetchAll($idMedicalRecord)
    {
        return DB::table('minutes')
            ->where('id_medical_record', $idMedicalRecord)->orderBy('dt_minute','DESC')->get();
    }

    public function insertMinute($params)
    {
        DB::beginTransaction();
        $return = DB::table('minutes')->insert($params);

        if ($return) {
            DB::commit();
            return [
                'success' => true,
                'message' => 'Sucesso ao salvar'
            ];
        } else {
            DB::rollback();
            return [
                'success' => false,
                'message' => 'Falha ao salvar'
            ];
        }
    }
}
