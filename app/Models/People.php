<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;


class People extends Model
{
//    protected $fillable = ['id_person'];
//    public $timestamps = false;

    public function register($person)
    {
        try {
            DB::beginTransaction();
            DB::table('people')->insert($person);
            $id = DB::getPdo()->lastInsertId();
            $this->registerAsClient($id);
            DB::commit();
            return [
                'success' => true,
                'message' => 'Sucesso ao salvar'
                ];
        } catch (Exception $e) {
            DB::rollback();
            return [
                'success' => false,
                'message' => 'Falha ao salvar'
            ];
        }
    }

    public function fetchAll()
    {
        return DB::table('people')
                    ->leftJoin('client', 'client.id_person', '=', 'people.id')
                    ->leftJoin('institutions', 'client.id_institution', '=', 'institutions.id')
                    ->where('client.id_institution', auth()->user()->id_institution)
                    ->get(['people.st_name AS st_name' , 'people.id']);
    }

    public function fetch($id)
    {
//        return DB::table('people')->where([['id_owner', auth()->user()->id],['id_animal',$id]])->first();
    }

    public function registerAsClient ($id) {
        DB::table('client')->insert(['id_institution'=> auth()->user()->id_institution, 'id_person'=> $id]);
    }
}
