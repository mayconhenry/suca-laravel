<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class City extends Model
{
    public function fetchAll()
    {
        return DB::table('cities')->get();
    }
}
