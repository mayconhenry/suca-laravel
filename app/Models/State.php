<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class State extends Model
{
    public function fetchAll()
    {
        return DB::table('states')->get();
    }
}
