<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Animals extends Model
{
    public function register($arrAnimal)
    {
        DB::beginTransaction();
        $return = DB::table('animals')->insert($arrAnimal);

        if ($return) {
            DB::commit();
            return [
                'success' => true,
                'message' => 'Sucesso ao salvar'
            ];
        } else {
            DB::rollback();
            return [
                'success' => false,
                'message' => 'Falha ao salvar'
            ];
        }
    }

    public function edit($animal)
    {
        DB::beginTransaction();
        $return = DB::table('animals')->where('id', $animal['id'])->update($animal);

        if ($return) {
            DB::commit();
            return [
                'success' => true,
                'message' => 'Sucesso ao salvar'
            ];
        } else {
            DB::rollback();
            return [
                'success' => false,
                'message' => 'Falha ao salvar'
            ];
        }
    }

    public function fetchAll()
    {
        return DB::table('animals')
            ->leftJoin('people', 'people.id', '=', 'animals.id_owner')
            ->leftJoin('client', 'client.id', '=', 'people.id')
            ->leftJoin('species', 'species.id', '=', 'animals.id_specie')
            ->leftJoin('breed', 'breed.id', '=', 'animals.id_breed')
            ->where('client.id_institution', auth()->user()->id_institution)
            ->get([
                'animals.id',
                'animals.st_name as st_name_animal',
                'people.st_name as st_name_owner',
                'species.st_name as no_specie',
                'breed.st_name as no_breed',
            ]);
    }

    public function fetch($id)
    {
        return DB::table('animals')
            ->leftJoin('people', 'people.id', '=', 'animals.id_owner')
            ->leftJoin('client', 'client.id', '=', 'people.id')
            ->leftJoin('species', 'species.id', '=', 'animals.id_specie')
            ->leftJoin('breed', 'breed.id', '=', 'animals.id_breed')
            ->where('client.id_institution', auth()->user()->id_institution)
            ->where([['animals.id',$id]])
            ->get([
                'animals.id',
                'animals.id_owner',
                'animals.st_name as st_name_animal',
                'people.st_name as st_name_owner',
                'people.nu_phone',
                'animals.bo_alive',
                'animals.st_pedigree',
                'animals.st_chip',
                'animals.id_specie',
                'animals.id_breed',
                'animals.st_postage',
                'animals.nu_weight',
                'animals.id_sexo',
                'animals.bo_castrated',
                'animals.st_color',
                'animals.st_coat',
                'animals.dt_birth',
                'species.st_name as no_specie',
                'breed.st_name as no_breed',
                'species.id as id_specie'
            ])->first();
    }
}
