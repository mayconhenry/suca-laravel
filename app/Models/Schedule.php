<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Schedule extends Model
{

    public function fetchAll($id)
    {
        return DB::table('schedule')
            ->leftJoin('service_type', 'schedule.id_type', '=', 'service_type.id')
            ->where('id_animal', $id)
            ->where('in_ativo', true)
            ->orderByRaw('schedule.dt_service ASC')
            ->get([
                'schedule.id',
                'schedule.st_note',
                'schedule.dt_service',
                'schedule.bo_notification',
                'service_type.st_name as no_type'
            ]);
    }

    public function fetch($id)
    {
        return DB::table('schedule')
            ->leftJoin('service_type', 'schedule.id_type', '=', 'service_type.id')
            ->where('schedule.id', $id)
            ->where('in_ativo', true)
            ->get([
                'schedule.id',
                'schedule.st_note',
                'schedule.dt_service',
                'schedule.id_animal',
                'schedule.id_type',
                'schedule.bo_notification',
                'service_type.st_name as no_type'
            ])
            ->first();
    }

    public function archiveSchedule ($id)
    {
        try {
            DB::beginTransaction();
            DB::table('schedule')
                ->where('id', $id)
                ->update(['in_ativo'=> false]);
            DB::commit();
            return [
                'success' => true,
                'message' => 'Sucesso ao salvar'
            ];
        } catch (Exception $e) {
            DB::rollback();
            return [
                'success' => false,
                'message' => 'Falha ao salvar'
            ];
        }
    }

    public function deleteSchedule($id)
    {
        try {
            DB::beginTransaction();
            DB::table('schedule')
                ->where('id', $id)
                ->update(['in_ativo'=> false]);
            DB::commit();
            return [
                'success' => true,
                'message' => 'Sucesso ao salvar'
            ];
        } catch (Exception $e) {
            DB::rollback();
            return [
                'success' => false,
                'message' => 'Falha ao salvar'
            ];
        }
    }

    public function register($schedule)
    {
        try {
            DB::beginTransaction();
            DB::table('schedule')->insert($schedule);
            DB::commit();
            return [
                'success' => true,
                'message' => 'Sucesso ao salvar'
            ];
        } catch (Exception $e) {
            DB::rollback();
            return [
                'success' => false,
                'message' => 'Falha ao salvar'
            ];
        }
    }

    public function listAll()
    {
        return DB::table('schedule')
            ->leftJoin('service_type', 'schedule.id_type', '=', 'service_type.id')
            ->leftJoin('animals', 'animals.id', '=', 'schedule.id_animal')
            ->leftJoin('people' , 'people.id', '=', 'animals.id_owner')
            ->where('in_ativo', true)
            ->orderByRaw('schedule.dt_service ASC')
            ->get([
                'schedule.id',
                'schedule.st_note',
                'schedule.dt_service',
                'schedule.bo_notification',
                'service_type.st_name as no_type',
                'animals.st_name as no_animal',
                'animals.id as id_animal',
                'people.st_name as no_owner',
                'people.id as id_owner',
                'people.nu_phone'
            ]);
    }
}
