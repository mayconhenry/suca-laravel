<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class Vaccine extends Model
{
    public function fetchAll($idAnimal)
    {
        return DB::table('vaccine')->where('id_animal', $idAnimal)->get();
    }

    public function register($vaccine)
    {
        DB::beginTransaction();
        $return = DB::table('vaccine')->insert($vaccine);

        if ($return) {

            DB::commit();

            return [
                'success' => true,
                'message' => 'Sucesso ao salvar'
            ];
        } else {

            DB::rollback();

            return [
                'success' => false,
                'message' => 'Falha ao salvar'
            ];
        }
    }
}
