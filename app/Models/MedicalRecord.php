<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class MedicalRecord extends Model
{
    public function fetch($id)
    {
        return DB::table('medical_record')
            ->where([['id_animal',$id],['in_ativo',true]])->first();
    }
}
