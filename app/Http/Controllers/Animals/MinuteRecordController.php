<?php

namespace App\Http\Controllers\Animals;

use App\Models\Minutes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MinuteRecordController extends Controller
{
    public function save($idMedicalRecord, Request $request, Minutes $minutes)
    {
        //monta data para gravar no banco
        $date = str_replace('/', '-', $request->date);
        $newDate = date("Y-m-d", strtotime($date));
        $strDatetime = strtotime($newDate . " " .  $request->time);

        $params['dt_minute'] = date("Y-m-d H:i:s", $strDatetime);
        $params['st_abstract'] = $request->st_abstract;
        $params['st_description'] = $request->st_description;
        $params['id_medical_record'] = $idMedicalRecord;

        $minutes->insertMinute($params);
    }

    public function list($idMedicalRecord, Minutes $minute)
    {
        $minutes = $minute->fetchAll($idMedicalRecord);
        return response()->json($minutes, 200);

    }
}
