<?php

namespace App\Http\Controllers\Animals;

use App\Models\Service;
use App\Models\ServiceType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    // carrega a pagina de atendimento do animal
    public function index ($id = null, Service $mService, ServiceType $mServiceType)
    {
        $arrService = $mService->fetchAll($id)->toArray();
        $arrServiceType = $mServiceType->fetchAll()->toArray();
        return view('animals.service', compact('arrServiceType', 'arrService', 'id'));
    }

    // registra atendimento de um animal
    public function save ($id = null, Request $request, Service $mService)
    {
        $params['id_animal']  = $id;
        $params['id_type']    = $request->id_type;
        $params['st_note']    = $request->st_note;
        $mService->register($params);
    }

    public function search( $id, Service $service)
    {
        $services = $service->fetchAll($id);
        return response()->json($services, 200);
    }
}
