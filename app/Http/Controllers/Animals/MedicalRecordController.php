<?php

namespace App\Http\Controllers\Animals;

use App\Models\Minutes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MedicalRecordController extends Controller
{
    public function saveMinute($id, $idMedicalRecord, Request $request, Minutes $minutes)
    {
        //monta data para gravar no banco
        $date = str_replace('/', '-', $request->date);
        $newDate = date("Y-m-d", strtotime($date));
        $strDatetime = strtotime($newDate . " " .  $request->time);

        $params['dt_minute'] = date("Y-m-d H:i:s", $strDatetime);
        $params['st_abstract'] = $request->st_abstract;
        $params['st_description'] = $request->st_description;
        $params['id_medical_record'] = $idMedicalRecord;

        $minutes->insertMinute($params);
    }
}
