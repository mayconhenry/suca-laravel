<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicalRecordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('medical_record', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_animal');
            $table->dateTime('dt_arrival')->nullable();
            $table->date('dt_bullish_forecast')->nullable();
            $table->boolean('bo_awake')->nullable();
            $table->float('nu_weight',  8, 3)->nullable();
            $table->integer('id_box')->nullable();
            $table->string('ds_note')->nullable();
            $table->boolean('in_ativo')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medical_record');
    }
}
