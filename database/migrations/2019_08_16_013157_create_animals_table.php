<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_owner');
            $table->string('st_name');
            $table->boolean('bo_alive');
            $table->string('st_pedigree')->nullable();
            $table->string('st_chip')->nullable();
            $table->integer('id_specie')->nullable();
            $table->integer('id_breed')->nullable();
            $table->string('st_postage')->nullable();
            $table->float('nu_weight',  8, 3)->nullable();
            $table->integer('id_sexo')->nullable();
            $table->boolean('bo_castrated')->nullable();
            $table->string('st_color')->nullable();
            $table->string('st_coat')->nullable();
            $table->date('dt_birth')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animals');
    }
}