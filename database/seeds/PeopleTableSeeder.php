<?php

use Illuminate\Database\Seeder;

class PeopleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('people')->insert(['id' => 1,'st_name' => 'Michael Henrique', 'nu_phone'=> '(61)984135094']);
        DB::table('people')->insert(['id' => 2,'st_name' => 'Katia Pereira']);
    }
}
