<?php

use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('service')->insert([
            'id_animal' => 1,
            'id_type'   => 1,
            'st_note'   => 'A vacina 1/3 do animal foi dada',
            'dt_service'=> '2000-01-01',
        ]);
        DB::table('service')->insert([
            'id_animal' => 1,
            'id_type'   => 1,
            'st_note'   => 'A vacina 2/3 do animal foi dada',
            'dt_service'=> '2000-02-01',
        ]);
    }
}
