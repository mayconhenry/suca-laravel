<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([UsersTableSeeder::class]);
        $this->call([AnimalTableSeeder::class]);
        $this->call([BreedTableSeeder::class]);
        $this->call([SpecieTableSeeder::class]);
        $this->call([StateTableSeeder::class]);
        $this->call([CityTableSeeder::class]);
        $this->call([PeopleTableSeeder::class]);
        $this->call([InstititionsTableSeeder::class]);
        $this->call([ClientTableSeeder::class]);
        $this->call([ServiceTypeSeeder::class]);
        $this->call([ServiceSeeder::class]);
        $this->call([ScheduleTableSeeder::class]);
        $this->call([MedicalRecordTableSeeder::class]);
        $this->call([MinutesTableSeeder::class]);
    }
}
