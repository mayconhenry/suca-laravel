<?php

use Illuminate\Database\Seeder;

class BreedTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'AKITA']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'AMERICAN BULLY']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'AMERICAN STAFFORDSHIRE']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'BASSET HOUND']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'BEAGLE']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'BICHON FRISE']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'BLUE HILLER']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'BORDER COLLIE']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'BOXER']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'BULDOG CAMPEIRO']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'BULDOG FRANCES']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'BULDOG INGLES']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'BULL TERRIER']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'BULLDOG AMERICANO']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'CÃO DE CRISTA CHINES']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'CAVALIER']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'CHIHUAHUA']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'CHOW CHOW']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'COCKER']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'COLIE']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'COTON DE TULEAR']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'DACHSHUND']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'DALMATA']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'DOBERMAN']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'DOGO ARGENTINO']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'DOGUE ALEMÃO']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'DOGUE DE BORDO']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'FILA BRASILEIRA']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'FOX PAULISTINHA']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'GOLDEN RETRIVER']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'HUSKY SIBERIANO']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'JACK RUSSEL']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'LABRADOR']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'LEÃO DA RODÉSIA']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'LHASA APSO']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'MALTES']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'MASTIN NAPOLITANO']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'NORFOLK TERRIER']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'PAPILON']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'PASTOR ALEMÃO']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'PASTOR BELGA']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'PASTOR DE MALINOIS']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'PASTOR MAREMANO']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'PASTOR SUIÇO']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'Pinscher']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'PINSCHER ']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'PIQUINES']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'PIT BULL']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'PIT MONSTER']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'PITBULL']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'POODLE']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'PUG']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'ROTTWEILER']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'SAMOIEDA']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'SÃO BERNARDO']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'SCHNAUZER']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'SCOTT TERRIER']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'SHARPEI']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'SHIHTZU']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'SHIHZU']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'SPINNER JAPONES']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'SPITZ ALEMÃO']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'SRD']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'SRD GRANDE']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'SRD MEDIO']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'SRD PEQUENO PORTE']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'TECKEL']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'WEIMARANER']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'WEST TERRIER']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'WHIPPET']);
        DB::table('breed')->insert(['id_specie' => 1, 'st_name' => 'YORKSHIRE']);
        DB::table('breed')->insert(['id_specie' => 2, 'st_name' => 'ANGORA TURCO']);
        DB::table('breed')->insert(['id_specie' => 2, 'st_name' => 'GATO RUSSO']);
        DB::table('breed')->insert(['id_specie' => 2, 'st_name' => 'IMALAIA']);
        DB::table('breed')->insert(['id_specie' => 2, 'st_name' => 'Maine Coon']);
        DB::table('breed')->insert(['id_specie' => 2, 'st_name' => 'PERSA']);
        DB::table('breed')->insert(['id_specie' => 2, 'st_name' => 'shih ']);
        DB::table('breed')->insert(['id_specie' => 2, 'st_name' => 'Siames']);
        DB::table('breed')->insert(['id_specie' => 2, 'st_name' => 'SRD']);
    }
}




