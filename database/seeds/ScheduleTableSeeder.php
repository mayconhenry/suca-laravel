<?php

use Illuminate\Database\Seeder;

class ScheduleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('schedule')->insert([
            'id_animal' => 1,
            'id_type'   => 1,
            'st_note'   => 'A vacina 1/3 do animal foi dada',
            'bo_notification'=> true,
            'in_ativo' => true,
            'dt_service'=> '2000-01-01',
        ]);
        DB::table('schedule')->insert([
            'id_animal' => 2,
            'id_type'   => 1,
            'st_note'   => 'A vacina 2/3 do animal foi dada',
            'bo_notification'=> false,
            'in_ativo' => true,
            'dt_service'=> '2000-02-01',
        ]);
        DB::table('schedule')->insert([
            'id_animal' => 3,
            'id_type'   => 1,
            'st_note'   => 'A vacina 2/3 do animal foi dada',
            'bo_notification'=> true,
            'in_ativo' => true,
            'dt_service'=> '2000-02-01',
        ]);
        DB::table('schedule')->insert([
            'id_animal' => 4,
            'id_type'   => 1,
            'st_note'   => 'A vacina 2/3 do animal foi dada',
            'bo_notification'=> false,
            'in_ativo' => true,
            'dt_service'=> '2000-02-01',
        ]);
    }
}
