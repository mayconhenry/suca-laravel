<?php

use Illuminate\Database\Seeder;

class MinutesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('minutes')->insert([
            'id_medical_record' => 1,
            'st_abstract'   => 'abstract',
            'st_description'   => 'A vacina 1111 do animal foi dada',
            'in_ativo'=> true,
        ]);
        DB::table('minutes')->insert([
            'id_medical_record' => 1,
            'st_abstract'   => 'abstract',
            'st_description'   => 'A vacina 2222 do animal foi dada',
            'in_ativo'=> true,
        ]);
        DB::table('minutes')->insert([
            'id_medical_record' => 1,
            'st_abstract'   => 'abstract',
            'st_description'   => 'A vacina 3333 do animal foi dada',
            'in_ativo'=> true,
        ]);
    }
}
