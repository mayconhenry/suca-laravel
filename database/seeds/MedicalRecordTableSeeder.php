<?php

use Illuminate\Database\Seeder;

class MedicalRecordTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('medical_record')->insert([
            'id' => 1,
            'id_animal'   => 1,
            'dt_arrival'=> new DateTime(),
            'dt_bullish_forecast'=> new DateTime(),
            'bo_awake'   => true,
            'nu_weight'=> 13.50,
            'id_box' => 1,
            'ds_note'=> 'Animal vomitando',
            'in_ativo'=> true,
        ]);
    }
}
