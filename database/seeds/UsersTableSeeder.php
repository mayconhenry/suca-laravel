<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Maycon Henry',
            'email' => 'mayconhenry@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('asdfasdf'),
            'created_at' => now(),
            'updated_at' => now(),
            'id_institution'=> 1
        ]);

        DB::table('users')->insert([
            'name' => 'Kátia Pereira',
            'email' => 'katia@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('asdfasdf'),
            'created_at' => now(),
            'updated_at' => now(),
            'id_institution'=> 2
        ]);
    }
}
