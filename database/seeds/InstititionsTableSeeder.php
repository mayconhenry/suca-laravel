<?php

use Illuminate\Database\Seeder;

class InstititionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('institutions')->insert(['id' => 1, 'st_name' => 'PetShop']);
    }
}
