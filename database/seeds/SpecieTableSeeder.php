<?php

use Illuminate\Database\Seeder;

class SpecieTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('species')->insert(['id' => 1, 'st_name' => 'Canina']);
        DB::table('species')->insert(['id' => 2, 'st_name' => 'Felina']);
        DB::table('species')->insert(['id' => 3, 'st_name' => 'Não definida']);
    }
}
