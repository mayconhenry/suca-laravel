<?php

use Illuminate\Database\Seeder;

class ServiceTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('service_type')->insert(['id' => 1, 'st_name' => 'Vacina']);
        DB::table('service_type')->insert(['id' => 2, 'st_name' => 'Banho/Tosa']);
        DB::table('service_type')->insert(['id' => 3, 'st_name' => 'Troca de coleira']);
    }
}
