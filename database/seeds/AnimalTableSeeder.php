<?php

use Illuminate\Database\Seeder;

class AnimalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('animals')->insert([
            'id_owner'  => 1,
            'st_name'   => 'Mel',
            'bo_alive'  => true,
            'st_pedigree' => 'pedigree',
            'st_chip' => 'chip',
            'id_specie' => 1,
            'id_breed' => 1,
            'st_postage' => 'postage',
            'nu_weight' => 100,
            'id_sexo'=> 1,
            'bo_castrated' => true,
            'st_color' => 'color',
            'st_coat' => 'coat',
            'dt_birth' => '2000-01-01',
        ]);
        DB::table('animals')->insert([
            'id_owner'  => 1,
            'st_name'   => 'Simba',
            'bo_alive'  => true,
            'st_pedigree' => 'pedigree',
            'st_chip' => 'chip',
            'id_specie' => 1,
            'id_breed' => 1,
            'st_postage' => 'postage',
            'nu_weight' => 100,
            'id_sexo'=> 1,
            'bo_castrated' => true,
            'st_color' => 'color',
            'st_coat' => 'coat',
            'dt_birth' => '2000-01-01',
        ]);
        DB::table('animals')->insert([
            'id_owner'  => 1,
            'st_name'   => 'Cook',
            'bo_alive'  => true,
            'st_pedigree' => 'pedigree',
            'st_chip' => 'chip',
            'id_specie' => 1,
            'id_breed' => 1,
            'st_postage' => 'postage',
            'nu_weight' => 100,
            'id_sexo'=> 1,
            'bo_castrated' => true,
            'st_color' => 'color',
            'st_coat' => 'coat',
            'dt_birth' => '2000-01-01',
        ]);
        DB::table('animals')->insert([
            'id_owner'  => 1,
            'st_name'   => 'Barner',
            'bo_alive'  => true,
            'st_pedigree' => 'pedigree',
            'st_chip' => 'chip',
            'id_specie' => 1,
            'id_breed' => 1,
            'st_postage' => 'postage',
            'nu_weight' => 100,
            'id_sexo'=> 1,
            'bo_castrated' => true,
            'st_color' => 'color',
            'st_coat' => 'coat',
            'dt_birth' => '2000-01-01',
        ]);
        DB::table('animals')->insert([
            'id_owner'  => 1,
            'st_name'   => 'Lucy',
            'bo_alive'  => true,
            'st_pedigree' => 'pedigree',
            'st_chip' => 'chip',
            'id_specie' => 1,
            'id_breed' => 1,
            'st_postage' => 'postage',
            'nu_weight' => 100,
            'id_sexo'=> 1,
            'bo_castrated' => true,
            'st_color' => 'color',
            'st_coat' => 'coat',
            'dt_birth' => '2000-01-01',
        ]);
        DB::table('animals')->insert([
            'id_owner'  => 1,
            'st_name'   => 'Lulu',
            'bo_alive'  => true,
            'st_pedigree' => 'pedigree',
            'st_chip' => 'chip',
            'id_specie' => 1,
            'id_breed' => 1,
            'st_postage' => 'postage',
            'nu_weight' => 100,
            'id_sexo'=> 1,
            'bo_castrated' => true,
            'st_color' => 'color',
            'st_coat' => 'coat',
            'dt_birth' => '2000-01-01',
        ]);

        DB::table('animals')->insert([
            'id_owner'  => 2,
            'st_name'   => 'Mel2',
            'bo_alive'  => true,
            'st_pedigree' => 'pedigreee',
            'st_chip' => 'chipa',
            'id_specie' => 1,
            'id_breed' => 1,
            'st_postage' => 'postagee',
            'nu_weight' => 100,
            'id_sexo'=> 1,
            'bo_castrated' => true,
            'st_color' => 'color',
            'st_coat' => 'coat',
            'dt_birth' => '2000-01-01',
        ]);
    }
}
