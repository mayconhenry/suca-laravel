<?php

use Illuminate\Database\Seeder;

class ClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('client')->insert(['id' => 1, 'id_person' => 1, 'id_institution'=> 1]);
        DB::table('client')->insert(['id' => 2, 'id_person' => 2, 'id_institution'=> 2]);
    }
}
